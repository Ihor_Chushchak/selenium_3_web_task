package po;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import utils.ConfigReader;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class GmailAndAuthorizationPageTest {

    private static final Logger LOG = LogManager.getLogger(GmailAndAuthorizationPageTest.class);
    private static WebDriver driver;
    private static  WebDriverWait wait;

    @BeforeClass
    static void initializeObjects() {
        LOG.info("Open web-driver");
        System.setProperty(ConfigReader.read("WEB_DRIVER_NAME"),ConfigReader.read("PATH_TO_DRIVER"));
        driver = new ChromeDriver();
        driver.manage().timeouts()
                .implicitlyWait(Long.parseLong(ConfigReader.read("DEFAULT_IMPLICITLY_WAIT_TIME")), TimeUnit.SECONDS);
        LOG.info("Moving to login_submit page");
        driver.get(ConfigReader.read("HOME_PAGE"));
    }

    @Test
    void openGmailAndLoginTest(){
        GmailAndAuthorizationPage gmailLoginPage = new GmailAndAuthorizationPage(driver);
        LOG.info("Entering login");
        gmailLoginPage.fillLogin(ConfigReader.read("EMAIL_LOGIN"));
        LOG.info("Moving to password_submit page");
        gmailLoginPage.clickLoginNextButtom();
        GmailAndAuthorizationPage gmailPaswordPage = new GmailAndAuthorizationPage(driver);
        LOG.info("Entering password");
        gmailPaswordPage.fillPassword(ConfigReader.read("EMAIL_PASSWORD"));
        LOG.info("Moving to gmail_main page");
        gmailPaswordPage.clickPasswordNextButton(driver,wait);
        assertEquals( driver.findElement(By.cssSelector("#\\:iv > div > div")).getText(),"Написати");
    }

    @Test(dependsOnMethods = {"openGmailAndLoginTest"})
    void clickComposeButtonTest(){
        GmailAndAuthorizationPage gmailPage = new GmailAndAuthorizationPage(driver);
        LOG.info("Clicking compose_email button");
        gmailPage.clickComposeEmailButton();
        assertEquals(driver.findElement(By.xpath("/html/body/div[19]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[1]/div/h2/div[2]")).getText(),"Нове повідомлення");
    }

    @Test(dependsOnMethods = {"clickComposeButtonTest"})
    void fillToAndSubjectAndMessageFieldsTest(){
        GmailAndAuthorizationPage gmailMessagePage = new GmailAndAuthorizationPage(driver);
        LOG.info("Filling \"To\" filed");
        gmailMessagePage.fillToField(ConfigReader.read("EMAIL_LOGIN"));
        LOG.info("Filling \"Subject\" filed");
        gmailMessagePage.fillSubjectField(ConfigReader.read("EMAIL_SUBJECT"));
        LOG.info("Filling \"Message\" filed");
        gmailMessagePage.fillMessageField(ConfigReader.read("EMAIL_MESSAGE"));
        assertEquals(driver.findElement(By.cssSelector("#\\:kx > h2 > div.aYF")).getText(),ConfigReader.read("EMAIL_SUBJECT"));
    }

    @Test(dependsOnMethods = {"fillToAndSubjectAndMessageFieldsTest"})
    void clickSendButtonTest(){
        GmailAndAuthorizationPage gmailMessagePage = new GmailAndAuthorizationPage(driver);
        LOG.info("Clicking send_email button");
        gmailMessagePage.clickSendEmailButton();
        assertEquals(driver.findElement(By.xpath("//*[@id='link_vsm']")).getText(),"Переглянути повідомлення");
    }

    @Test(dependsOnMethods = {"clickSendButtonTest"})
    void verifyThatMessageIsInSentFolderTest(){
        GmailAndAuthorizationPage gmailActualMessagePage = new GmailAndAuthorizationPage(driver);
        LOG.info("Clicking open_sended_email button");
        gmailActualMessagePage.clickOpenMessageButton();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime now = LocalDateTime.now();
        assertEquals(dtf.format(now) + " (0 хвилин тому)", driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]" +
                "/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[2]/div" +
                "/div[3]/div/div/div/div/div/div[1]/div[2]/div[1]/table/tbody/tr[1]/td[2]/div/span[2]")).getText());
        assertEquals(gmailActualMessagePage.checkSendButton(),"0");
    }

    @Test(dependsOnMethods = {"verifyThatMessageIsInSentFolderTest"})
    void  removeMessageFromTheSentFolderTest(){
        GmailAndAuthorizationPage gmailActualMessagePage = new GmailAndAuthorizationPage(driver);
        LOG.info("Clicking delete_sended_email button");
        gmailActualMessagePage.clickDeleteEmailButton();
        assertEquals(driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[1]/div[4]/div[1]/div/div[3]/div/div/div[2]/span/span[1]")).getText(),"Ланцюжок повідомлень переміщено в кошик.");
    }

    @AfterClass
    static void closeResources() {
        driver.quit();
    }

}


