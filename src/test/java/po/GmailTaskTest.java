package po;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;
import utils.ConfigReader;

import static org.testng.Assert.assertEquals;

public class GmailTaskTest {

    private static final Logger LOG = LogManager.getLogger(GmailTaskTest.class);
    private static WebDriver driver;
    private static WebDriverWait wait;

    @BeforeClass
    static void initializeObjects() {
        System.setProperty(ConfigReader.read("WEB_DRIVER_NAME"),ConfigReader.read("PATH_TO_DRIVER"));
        driver = new ChromeDriver();
        driver.manage().timeouts()
                .implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(ConfigReader.read("HOME_PAGE"));
    }

    @Test
    void openGmailAndLoginTest(){
        GmailTask gmailLoginPage = new GmailTask(driver);
        gmailLoginPage.fillLogin(ConfigReader.read("EMAIL_LOGIN"));
        gmailLoginPage.clickLoginNextButtom();
        GmailTask gmailPaswordPage = new GmailTask(driver);
        gmailPaswordPage.fillPassword(ConfigReader.read("EMAIL_PASSWORD"));
        gmailPaswordPage.clickPasswordNextButton(driver,wait);
        assertEquals( driver.findElement(By.cssSelector("#\\:iv > div > div")).getText(),"Написати");
    }

    @Test(dependsOnMethods = {"openGmailAndLoginTest"})
    void clickComposeButtonTest(){
        GmailTask gmailPage = new GmailTask(driver);
        gmailPage.clickComposeEmailButton();
    }

    @Test(dependsOnMethods = {"clickComposeButtonTest"})
    void fillToAndSubjectAndMessageFieldsTest(){
        GmailTask gmailMessagePage = new GmailTask(driver);
        gmailMessagePage.fillToField(ConfigReader.read("EMAIL_LOGIN"));
        gmailMessagePage.fillSubjectField(ConfigReader.read("EMAIL_SUBJECT"));
        gmailMessagePage.fillMessageField(ConfigReader.read("EMAIL_MESSAGE"));
    }

    @Test(dependsOnMethods = {"fillToAndSubjectAndMessageFieldsTest"})
    void clickSendButtonTest(){
        GmailTask gmailMessagePage = new GmailTask(driver);
        gmailMessagePage.clickSendEmailButton();
    }

    @Test(dependsOnMethods = {"clickSendButtonTest"})
    void verifyThatMessageIsInSentFolderTest(){
        GmailTask gmailActualMessagePage = new GmailTask(driver);
        gmailActualMessagePage.clickOpenMessageButton();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime now = LocalDateTime.now();
        assertEquals(dtf.format(now) + " (0 хвилин тому)", driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]" +
                "/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div/table/tr/td[1]/div[2]/div[2]/div" +
                "/div[3]/div/div/div/div/div/div[1]/div[2]/div[1]/table/tbody/tr[1]/td[2]/div/span[2]")).getText());
       assertEquals(gmailActualMessagePage.checkSendButton(),"0");
    }

    @Test(dependsOnMethods = {"verifyThatMessageIsInSentFolderTest"})
    void  removeMessageFromTheSentFolderTest(){
        GmailTask gmailActualMessagePage = new GmailTask(driver);
        gmailActualMessagePage.clickDeleteEmailButton();
    }

    @AfterClass
    static void closeResources() {
        driver.quit();
    }

}


