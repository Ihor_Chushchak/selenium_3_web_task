package po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailAndAuthorizationPage{

    @FindBy(css = "#identifierId")
    private WebElement loginInput;

    @FindBy(xpath = "//*[@id=\"identifierNext\"]/span")
    private WebElement loginNextButton;

    @FindBy(xpath = "//*[@id=\"password\"]/div[1]/div/div[1]/input")
    private WebElement passwordInput;

    @FindBy(css = "#passwordNext")
    private WebElement passwordNextButton;

    @FindBy(css = "#\\:iv > div > div")
    private WebElement composeEmailButton;

    @FindBy(css = "textarea[name='to']")
    private WebElement toField;

    @FindBy(css = "input[name='subjectbox']")
    private WebElement subjectField;

    @FindBy(xpath = "//div[@class='Ar Au']/*[@role='textbox']")
    private WebElement messageField;

    @FindBy(xpath = "//div[@class='dC']/*[@role='button']")
    private WebElement sendEmailButton;

    @FindBy(xpath = "//*[@id='link_vsm']")
    private WebElement openMessageButton;

    @FindBy(xpath = "//*[@id=\":j7\"]/div/div[2]/span/a")
    private WebElement chooseSendButton;

    @FindBy(xpath = "//*[@id=\":4\"]/div[2]/div[1]/div/div[2]/div[3]")
    private WebElement deleteEmailButton;

    public GmailAndAuthorizationPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void fillLogin(String login) {
        loginInput.sendKeys(login);
    }

    public void clickLoginNextButtom() {
        loginNextButton.click();
    }

    public void fillPassword(String password) {
        passwordInput.sendKeys(password);
    }

    public void clickPasswordNextButton(WebDriver driver,WebDriverWait wait) {
        wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(passwordNextButton));
        passwordNextButton.click();
    }

    public void clickComposeEmailButton() {
        composeEmailButton.click();
    }

    public void fillToField(String to) {
        toField.sendKeys(to);
    }

    public void fillSubjectField(String subject) {
        subjectField.sendKeys(subject);
    }

    public void fillMessageField(String message) {
        messageField.sendKeys(message);
    }

    public void clickSendEmailButton() {
        sendEmailButton.click();
    }

    public void clickOpenMessageButton() {
        openMessageButton.click();
    }

    public String checkSendButton(){
     return chooseSendButton.getAttribute("tabindex");
    }

    public void clickDeleteEmailButton(){
        deleteEmailButton.click();
    }
}
